module.exports = {
    entry: './lib/client/main.js',
    output: {
        filename: './bundle.js'
    },
    target: 'web',
    node: {
        fs: "empty",
        module: "empty"
    }
};