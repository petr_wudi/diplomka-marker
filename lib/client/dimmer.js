const $ = require("jquery");

export function Dimmer(rangeElem, dimLayerElem) {
    this.rangeElem = rangeElem;
    const self = this;
    $(rangeElem).bind('click', function(event) { self.updateLayer() });
    this.dimLayerElem = dimLayerElem;
    this.updateLayer();
}

Dimmer.prototype.updateLayer = function() {
    const val = parseInt(this.rangeElem.value) + 1;
    const max = parseInt(this.rangeElem.max) + 1;
    this.dimLayerElem.style.opacity = (max - val)/max;
};