const $ = require("jquery");
const Image = require("./image").Image;
const Dimmer = require("./dimmer").Dimmer;

const colors = ['red', 'green', 'blue', 'cyan', 'yellow'];
let activeColor = null;
let image = undefined;

function loadImage() {
    const imageContainer = document.getElementById("imgWrapper");
    const imageCloseContainer = document.getElementById("imgCloserWrapper");
    const imageElem = document.getElementById("microscopeShot");
    const imgWidth = imageElem.scrollWidth;
    const imgHeight = imageElem.scrollHeight;
    image = new Image(imageElem, imageCloseContainer, imageContainer, imgWidth, imgHeight);
}

function chooseColor(color, element) {
    if (activeColor !== null) {
        activeColor.classList.remove('active');
    }
    activeColor = element;
    element.classList.add('active');
    // circles = document.getElementsByClassName('circle');
    // .forEach(function(circle) { // todo have all circles in an array
    //     circle.style.borderColor = color;
    // });
}

function initColors() {
    const container = document.getElementById("colors");
    colors.forEach(function(color, index) {
        const element = document.createElement("a");
        element.onclick = function() { chooseColor(color, element); };
        element.style.background = color;
        container.appendChild(element);
        if (index === 0) {
            chooseColor(color, element);
        }
    });
}

function loadDimmer() {
    const rangeElem = document.getElementById("dimRange");
    const dimLayerElem = document.getElementById("dimLayer");
    const dimmer = new Dimmer(rangeElem, dimLayerElem);
}

function getImageName() {
    const currentUrlStr = document.URL;
    const currentUrl = new URL(currentUrlStr);
    return currentUrl.searchParams.get("imgSrc");
}

function loadInitialCircles() {
    const imgSrc = getImageName();
    $.ajax({
        url: 'getCircles',
        type: 'POST',
        data: {
            sourceImage: imgSrc
        },
        success: function(data){
            console.log('success');
            console.log(data);
            data.forEach(function(circleData) {
                const x = circleData.x;
                const y = circleData.y;
                image.addCircle(x, y);
            });
        }
    });
}

function loadCircles() {
    const circles = [];
    image.circles.forEach(function(circle) {
        if (!circle.isDeleted) {
            const x = circle.getX();
            const y = circle.getY();
            circles.push({ x: x, y: y });
        }
    });
    return circles;
}

function saveCircles() {
    const circles = loadCircles();
    const imageName = getImageName();
    const pageSize = 100;
    const numOfPages = Math.ceil(circles.length / pageSize);
    savePage(imageName, circles, 0, pageSize, numOfPages);
}

function savePage(imageName, circles, pageId, pageSize, numOfPages) {
    console.log('Sending page', pageId + 1, 'of', numOfPages);
    const startId = pageId * pageSize;
    const endId = Math.min(circles.length, startId + pageSize);
    const subCircles = circles.slice(startId, endId);
    $.ajax({
        url: 'saveCircles',
        type: 'POST',
        data: {
            fileName: imageName,
            circles: subCircles,
            page: pageId
        },
        success: function(data){
            if (data.returnStatus) {
                console.log('successfully saved - page ' + pageId + " of " + numOfPages);
                if (numOfPages === pageId + 1) {
                    alert(circles.length + " points were successfully saved");
                    window.location.replace("/");
                } else {
                    savePage(imageName, circles, pageId + 1, pageSize, numOfPages);
                }
            }
        }
    });
}

function refreshExportButton(exportButton) {
    const circles = loadCircles();
    const imageName = getImageName();
    const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(circles));
    exportButton.setAttribute("href", dataStr);
    exportButton.setAttribute("download", imageName + ".json");
}

function importFile(file) {
    console.log("import file");
    const reader = new FileReader();
    reader.onload = function(event) {
        const data = JSON.parse(event.target.result);
        console.log(data);
        image.deleteAllCircles();
        data.forEach(function(circleData) {
            const x = circleData.x;
            const y = circleData.y;
            image.addCircle(x, y);
        });
        const exportButton = document.getElementById("export-button");
        refreshExportButton(exportButton);
    };
    reader.readAsText(file, "utf-8");
}

function importFiles(event) {
    const files = event.target.files;
    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        importFile(file);
    }
}

function loadImportExportWindow() {
    const windowElem = document.getElementById("importExportWindow");
    const displayButton = document.getElementById("importExportShowButton");
    const closeButton = document.getElementById("importExportCloseButton");
    const exportButton = document.getElementById("export-button");
    const importInputElem = document.getElementById("import-inputfile");
    displayButton.onclick = function() {
        windowElem.classList.add('open');
        windowElem.classList.remove('closed');
        refreshExportButton(exportButton);
    };
    closeButton.onclick = function() {
        windowElem.classList.add('closed');
        windowElem.classList.remove('open');
    };
    importInputElem.addEventListener("change", importFiles, false);
}

window.onload = function() {
    const img = document.getElementById("microscopeShot");
    loadImage();
    image.closerContainerElem.onclick = function(event) { image.click(event) };
    initColors();
    loadDimmer();
    loadInitialCircles();
    loadImportExportWindow();

    $("#submitButton").bind("click", function(event) { saveCircles(); });
    // const saveButton = document.getElementById("submitButton");
    // saveButton.onclick = saveCircles;
};